# Infrastructure As Code

no one installs servers manually, thus `IaC` was created. We use it to setup and config the servers and applications

  - Containeration
    - LXC (for history)
    - docker
    - docker-compose

- Containerization Management
    - k8s
    - nomad (just for theory)

- Configuration Management
    - Ansible

- Infra Provision
    - Terraform

- Continius Integration
    - Jenkins
    - Gitlab-Ci

- Continius Deployment
    - Jenkins
    - Gitlab-Ci
